
import requests


class PokeAPIHandler:

    base_url = "http://pokeapi.co/api/v2"
    pokemon_endpoint = "/pokemon"
    pokemon_shape_endpoint = "/pokemon-shape"

    def get_list_of_pokemons(self, params=None):
        response = requests.get(self.base_url + self.pokemon_endpoint, params=params)
        assert response.status_code == 200
        return response

    def get_pokemon_shapes(self):
        response = requests.get(self.base_url + self.pokemon_shape_endpoint)
        return response

    def get_pokemon_shape_by_name(self, name):
        response = requests.get(self.base_url + self.pokemon_shape_endpoint + f"/{name}")
        return response
