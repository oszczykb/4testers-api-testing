import requests


class GoRestHandler:

    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"

    headers = {
        "Authorization": "Bearer 62c79aab9740ed794151eafb66777276957a98025423cc89af35a3a3f870ffc1"
    }

    def create_user(self, user_data, expected_status_code=201):
        response = requests.post(self.base_url + self.users_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def get_user(self, user_id, expected_status_code=200):
        response = requests.get(self.base_url + self.users_endpoint + f"/{user_id}", headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def update_user(self, user_id, user_data, expected_status_code=200):
        response = requests.put(self.base_url + self.users_endpoint + f"/{user_id}", json=user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def delete_user(self, user_id, expected_status_code=204):
        response = requests.delete(self.base_url + self.users_endpoint + f"/{user_id}", headers=self.headers)
        assert response.status_code == expected_status_code
        return response
