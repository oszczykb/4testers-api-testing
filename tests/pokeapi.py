from tests.utils.pokeapi_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def test_default_list_of_pokemons():
    response = pokeapi_handler.get_list_of_pokemons()
    response_body = response.json()

    # czy json nie jest pusty
    assert len(response_body["results"]) > 0

    # assert response_body["results"]
    # assert "results" in response_body -> check only if list exists

    assert response.status_code == 200

    assert response_body["count"] == 1279

    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_bytes = len(response.content)
    assert response_size_bytes < 100*1000

    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100


def test_pagination():
    params = {
        "limit": 10,
        "offset": 20
    }

    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()

    assert len(body["results"]) == params["limit"]

    splitted = (body["results"][0]["url"]).split("/")
    assert int(splitted[-2]) == (params["offset"] + 1)

    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset']+1}/"

    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"


def test_pokemon_shape():
    response = pokeapi_handler.get_pokemon_shapes()
    body = response.json()

    assert body["count"] == len(body["results"])

    element_3_name = body["results"][2]["name"]

    response_shape = pokeapi_handler.get_pokemon_shape_by_name(element_3_name)
    body_shape = response_shape.json()

    assert body_shape["id"] == 3




