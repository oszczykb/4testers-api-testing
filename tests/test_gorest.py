from faker import Faker
from utils.gorest_handler import GoRestHandler


faker = Faker()
gorest_handler = GoRestHandler()


def test_create_user():
    user_data = {
        "name": "Michael Jordan",
        "gender": "male",
        "email": f"{faker.company_email()}",
        "status": "active"
    }
    user = gorest_handler.create_user(user_data).json()
    created_user = gorest_handler.get_user(user["id"]).json()

    assert user["id"] == created_user["id"]
    assert user["name"] == created_user["name"]
    assert user["email"] == created_user["email"]
    assert user["status"] == created_user["status"]


def test_update_user():
    user_data = {
        "name": "Michael Jordan",
        "gender": "male",
        "email": f"{faker.company_email()}",
        "status": "active"
    }
    user = gorest_handler.create_user(user_data).json()
    created_user = gorest_handler.get_user(user["id"]).json()

    # update all properties
    modified_email = faker.company_email()
    user_data_modify_all = {
        "name": "Michalina Jordańska",
        "gender": "female",
        "email": modified_email,
        "status": "inactive"
    }
    updated_modify_all_user = gorest_handler.update_user(created_user["id"], user_data_modify_all).json()
    assert updated_modify_all_user["name"] == "Michalina Jordańska" and updated_modify_all_user["gender"] == "female" and \
           updated_modify_all_user["email"] == modified_email and updated_modify_all_user["status"] == "inactive"

    # update_one property
    user_data_modify_one = {
        "name": "Kierownik Budowy"
    }
    updated_modify_one_user = gorest_handler.update_user(created_user["id"], user_data_modify_one).json()
    assert updated_modify_one_user["name"] == "Kierownik Budowy"

    # update email
    modified_email_only = faker.company_email()
    user_data_modify_email = {
        "email": modified_email_only
    }
    updated_modify_email_only_user = gorest_handler.update_user(created_user["id"], user_data_modify_email).json()
    assert updated_modify_email_only_user["email"] == modified_email_only

    # update with same value
    updated_modify_email_duplicate = gorest_handler.update_user(created_user["id"], user_data_modify_email).json()
    assert updated_modify_email_duplicate["email"] == modified_email_only

    # update with invalid email
    user_data_modify_invalid_email = {
        "email": "a$@dj+++'."
    }
    response = gorest_handler.update_user(created_user["id"], user_data_modify_invalid_email, expected_status_code=422)
    assert not response.ok


def test_delete_user():
    user_data = {
        "name": "Michael Jordan",
        "gender": "male",
        "email": f"{faker.company_email()}",
        "status": "active"
    }
    user = gorest_handler.create_user(user_data).json()
    created_user = gorest_handler.get_user(user["id"]).json()

    # delete existing user
    response_delete = gorest_handler.delete_user(created_user["id"])
    assert response_delete.ok
    response_get = gorest_handler.get_user(created_user["id"], expected_status_code=404)
    assert not response_get.ok

    # delete non-existing user
    response_delete_nonexisting = gorest_handler.delete_user(created_user["id"], expected_status_code=404)
    assert not response_delete_nonexisting.ok




